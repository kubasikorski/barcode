#/usr/bin/python
import time, requests, sys, os, datetime
import sqlite3 as lite
import simplejson as json

class serverconnect:
	def request(self,mydata):
		conn = None
		conn = lite.connect('kody.db')
		c = conn.cursor()
		c.execute('insert into kody values (?,?,?,?)', (None,mydata,0,datetime.datetime.now()))
		conn.commit()
		r = requests.post('http://kubasikorski.pl/rasp/index.php', json={"kod": mydata})
		data = json.loads(r.text)
		if data['status']=='ok':
			c.execute('update kody set status=? where kod=?', (1,mydata))
			conn.commit()
		conn.close()
		return r