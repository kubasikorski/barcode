
#/usr/bin/python
import time
import requests
import sqlite3 as lite
import sys

con = None
try:
    con = lite.connect('kody.db')
    
    cur = con.cursor()    
    cur.execute('SELECT SQLITE_VERSION()')
    
    data = cur.fetchone()
    
    print "SQLite version: %s" % data                
    
except lite.Error, e:
    
    print "Error %s:" % e.args[0]
    sys.exit(1)
    
finally:
    
    if con:
        con.close()

r = requests.post('http://kubasikorski.pl/rasp/index.php', json={"kod": "test"})
print r.status_code;
print r.json();

