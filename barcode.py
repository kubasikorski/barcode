#/usr/bin/python
import time, requests, sys, os
import simplejson as json
from termios import tcflush, TCIFLUSH
from tools.connect import serverconnect
from tools.display import *
from pprint import pprint

def boot():
	lcd = HD44780()
	lcd.message(" Uruchamianie...")
	time.sleep(1)
	barcode()

def barcode():

	try:
		lcd = HD44780()
		lcd.message(" Oczekiwanie    \nna kod          ")
		mydata = raw_input()
	except EOFError:
		barcode()
	lcd.message(" Wysylam:          \n"+mydata)
	r = serverconnect().request(mydata)
	data = json.loads(r.text)
	# print "-----------------"
	# print "Status: "+(data['status'])
	# print "Odebrany kod: "+(data['code'])
	# print "-----------------"
	lcd.clear()
	lcd.message(" Status:"+data['status']+"     \n"+data['code'])
	time.sleep(2)
	tcflush(sys.stdin, TCIFLUSH)
	os.system('clear')
	barcode()

if __name__ == "__main__":
	boot()